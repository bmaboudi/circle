import numpy as np
#import gauss_hermit as gh
import high_fidelity as hf
import metropolis as metro

high_fid = hf.high_fidelity(num_out=10, num_modes=50)

model_params = {}
model_params['center'] = np.zeros(2)
model_params['height'] = np.random.uniform(4.0,6.0)

#model_params = {}
#model_params['center'] = [ 0.37227102, 0.16107161]
#model_params['radius'] = [0.3697733250291406]
#model_params['rhs_params'] = [ 0.60883105, 0.58702218, 0.35206842, 0.51902329]


high_fid.set_params(model_params)
high_fid.assemble_bilinear_matrix()
y = high_fid.solve_multiple()

mu = 0.0
sigma = 0.05

y_observed = y

mtr = metro.Metropolis(y_observed, sigma)

#print(model_params['center'])
#print(model_params['radius'])
mtr.metro_alg()
print(model_params['height'])

x = model_params['height']

mean = mtr.compute_mean()
print(mean)
print(mtr.compute_var())

high_fid.set_params(model_params,2)
high_fid.assemble_rhs()
high_fid.assemble_bilinear_matrix()
y = high_fid.solve()

torch_in = torch_x = torch.from_numpy( np.reshape(mean,[1,-1]) ).float()
y_pred = eval_func.eval_model( torch_in )

print(np.max( np.abs(y-y_observed) ))

x_pred = mean

#np.savez('results.npz',y_observed=y_observed,y=y,y_pred=y_pred, x=x, x_pred=x_pred)

#mean_vec = np.zeros((1,8))
#var_vec = np.zeros((1,8))
#
#for i in range(0,1):
#    print('iteration :', (i+1))
#    mtr.metro_alg()
#    mean = mtr.compute_mean()
#    var = mtr.compute_var()
#    mean_vec[i,:] = mean
#    var_vec[i,:] = var
#
#
#file_name = 'result.npz'
#np.savez(file_name,params=model_params['blob_params'], mean_vec=mean_vec, var_vec=var_vec)
#
#print(mean)
#print(var)
