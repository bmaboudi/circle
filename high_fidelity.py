import numpy as np
from dolfin import *
from mshr import *

set_log_level(50)

def output_indices(x):
	return (x[0]*x[0] + x[1]*x[1] <  2) and (x[0]*x[0] + x[1]*x[1] >  1 - 3e-3)

class random_field_circle(UserExpression):
	def set_params(self,model_params):
		self.center = model_params['center']
		self.radius = 0.4
		self.height = model_params['height'][0]
	def eval(self, values, x):
		values[0] = 1.0
		r2 = self.radius*self.radius
		c = self.center
		if( (x[0] - c[0])*(x[0] - c[0]) + (x[1] - c[1])*(x[1] - c[1]) < r2 ):
			values[0] = self.height

class rhs(UserExpression):
	def set_mode(self, mode):
		self.mode = mode

	def eval(self, values, x):
		alpha = np.arctan( x[1]/x[0] )
		if(int(self.mode)%2 == 0):
			values[0] = 10*np.cos(self.mode*alpha)/np.sqrt(np.pi)
		else:
			values[0] = 10*np.sin(self.mode*alpha)/np.sqrt(np.pi)


class high_fidelity():
	def __init__(self, num_out = 100, num_modes=1):
		#domain = Circle(Point(0, 0), 1)
		#self.mesh = generate_mesh(domain, 40)
		#mesh_file = File("mesh.xml")
		#mesh_file << self.mesh
		self.mesh = Mesh('mesh.xml')
		V = FiniteElement("CG", self.mesh.ufl_cell(), 1)
		R = FiniteElement("R", self.mesh.ufl_cell(), 0)
		self.V_space = FunctionSpace(self.mesh, "CG", 1)
		self.W = FunctionSpace(self.mesh, V*R)
		(self.u, self.c) = TrialFunction(self.W)
		(self.v, self.d) = TestFunctions(self.W)
		
		FEM_el = self.V_space.ufl_element()
		self.g = rhs(element = FEM_el)
		#self.b_vec = np.transpose( np.matrix(b.get_local()) )

		self.alpha = random_field_circle(element = FEM_el)
		self.num_out = num_out
		self.num_modes = num_modes

		#self.rhs = rhs(element = FEM_el)
		#self.rhs.set_num_expansion(10)

	def set_params(self, model_params):
		self.alpha.set_params(model_params)
		self.g.set_mode(1.0)

	def assemble_rhs(self):
		self.L = self.g*self.v*ds
		b = assemble(self.L)

	def assemble_bilinear_matrix(self):
		#a = (inner(grad(self.alpha*self.u), grad(self.v)) + self.c*self.v + self.u*self.d)*dx
		self.a = self.alpha*self.u.dx(0)*self.v.dx(0)*dx + self.alpha*self.u.dx(1)*self.v.dx(1)*dx + self.c*self.v*ds + self.u*self.d*ds
		A = assemble(self.a)
		self.A_mat = np.matrix(A.array())

	def solve(self):
		self.w = Function(self.W)
		solve(self.a == self.L, self.w)
		(u, c) = self.w.split()

		#dummy = Function(self.V_space)
		#constant_dummy = Constant(1.0)
		#bc_dummy = DirichletBC(self.V_space, 1, output_indices)
		#bc_dummy.apply( dummy.vector() )
		#vec_dummy = np.squeeze( np.asarray( np.matrix( dummy.vector() ) ) )
		#idx = np.where(vec_dummy == 1)
		#idx = np.squeeze( np.asarray(idx) )
		#sol = np.squeeze( np.asarray( u.vector() ))
		#output_vec = sol[idx]
		#print(output_vec)

		k = np.reshape(list(range(self.num_out)),[-1])
		k = k/self.num_out
		z = 0.999*np.exp(1j*k*2*np.pi)

		out_vec = []
		for i in range(len(z)):
			out_vec.append( u(z[i].real,z[i].imag) )
		output_vec = np.reshape(out_vec,[-1])
		return output_vec

	def solve_multiple(self):
		k = np.reshape(list(range(self.num_out)),[-1])
		k = k/self.num_out
		z = 0.999*np.exp(1j*k*2*np.pi)

		out = []
		for mode in range(self.num_modes):
			self.g.set_mode(mode)
			self.assemble_rhs()

			self.w = Function(self.W)
			solve(self.a == self.L, self.w)
			(u, c) = self.w.split()

			out_local = []
			for i in range(len(z)):
				out_local.append( u(z[i].real,z[i].imag) )
			out.append( np.reshape(out_local,[-1]) )

		return np.reshape(out,[1,-1])

	def save_solution(self):
		(u, c) = self.w.split()
		file = File("solution.pvd")
		file << u

		#u = Function(self.V)
		#u.vector().set_local( self.solution )
		#file = File('solution.pvd')
		#file << u

	def save_random_field(self):
		alpha_func = interpolate(self.alpha, self.V_space)
		file = File('random_field.pvd')
		file << alpha_func

	#def check_boundary_value(self):
	#	self.w = Function(self.W)
	#	solve(self.a == self.L, self.w)
	#
	#	(u, c) = self.w.split()
	#	print( assemble(u*ds) )



if __name__=='__main__':
	hf = high_fidelity(num_out=10, num_modes=50)

	model_params = {}
	model_params['center'] = np.random.uniform(-0.5,0.5,2)
	model_params['height'] = [ np.random.uniform(4.0,6.0) ]

	hf.set_params(model_params)
	hf.assemble_rhs()
	hf.assemble_bilinear_matrix()
	hf.solve_multiple()
	#hf.solve()
	hf.save_solution()
	hf.save_random_field()