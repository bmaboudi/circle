import numpy as np
import high_fidelity as hf
import progressbar

def find_initial_state():
	x = np.random.uniform(4.0,6.0)
	return x

class Metropolis:
	def __init__(self, y_observed, sigma):
		self.y_observed = y_observed
		self.sigma2 = sigma*sigma
		self.epsilon = 2.22044604925e-16
		self.N = 1000

		self.hf = hf.high_fidelity(num_out=10, num_modes=50)

	def density_ratio(self, x, y):
		
		model_params = {}
		model_params['height'] = x
		self.hf.set_params(model_params)
		self.hf.assemble_rhs()
		self.hf.assemble_bilinear_matrix()
		fx = self.hf.solve_multiple()

		model_params['height'] = y
		self.hf.set_params(model_params)
		self.hf.assemble_rhs()
		self.hf.assemble_bilinear_matrix()
		fy = self.hf.solve_multiple()

		exponent = - np.inner(fy - self.y_observed, fy - self.y_observed) + np.inner(fx - self.y_observed, fx - self.y_observed)
		
		if(exponent/2/self.sigma2 > 1):
			ratio = 2.0
		else:
			ratio = np.exp(exponent/2/self.sigma2)
		return np.min([1.0, ratio])

	def checker(self, x):
		check = True 
		if(x < 4.0 or x > 6.0):
			check = False
		return check

	def metro_alg(self):
		x = find_initial_state()
		self.X = []
		self.X.append(x)

#		for i in range(0,self.N):
		for i in progressbar.progressbar(range(0, self.N)):
			y = np.random.uniform(0,1)
			y = x + 0.05*( y - 0.5 )
			if( self.checker(y) ):
				alpha = self.density_ratio(x, y)
				u = np.random.uniform(0,1)
				if( u < alpha ):
					x = y
					self.X.append(y)
				else:
					self.X.append(x)
			else:
				continue

	def compute_mean(self):
		N = len(self.X)
		samples = np.reshape( self.X, [N,-1] )
		mean = np.sum(samples,axis = 0)/N
		return mean

	def compute_var(self):
		N = len(self.X)
		mean = self.compute_mean()
		samples = np.reshape( self.X, [N,-1] )
		samples = samples - mean
		cov = np.matmul( samples.T, samples ) / N

		return np.sqrt(np.diagonal(cov))
